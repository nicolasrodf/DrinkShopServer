package com.example.nicolas.drinkshopserver;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nicolas.drinkshopserver.Model.Category;
import com.example.nicolas.drinkshopserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkshopserver.Utils.Common;
import com.example.nicolas.drinkshopserver.Utils.ProgressRequestBody;
import com.example.nicolas.drinkshopserver.Utils.UploadCallBack;
import com.ipaulpro.afilechooser.utils.FileUtils;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProductActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "UpdateProductActivity";
    private static final int PICK_FILE_REQUEST = 9999;

    ImageView browserImageView;
    EditText nameEditText, priceEditText, descriptionEditText;
    Button updateButton, deleteButton;

    IDrinkShopAPI mService;

    CompositeDisposable compositeDisposable;

    Uri selectedFileUri = null;
    String uploadedImgPath="", selected_category="";
    File compressedImageFile;

    MaterialSpinner menuSpinner;

    HashMap<String,String> menu_data_for_get_key = new HashMap<>();
    HashMap<String,String> menu_data_for_get_value = new HashMap<>();

    List<String> menu_data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);

        //View
        deleteButton = findViewById(R.id.delete_button);
        updateButton = findViewById(R.id.update_button);
        nameEditText = findViewById(R.id.drink_name_edit_text);
        priceEditText = findViewById(R.id.drink_price_edit_text);
        descriptionEditText = findViewById(R.id.drink_desc_edit_text);
        browserImageView = findViewById(R.id.browser_image_view);
        menuSpinner = findViewById(R.id.menu_id_spinner);

        //API
        mService = Common.getAPI();

        //RxJava
        compositeDisposable = new CompositeDisposable();

        if(Common.currentDrink != null){
            uploadedImgPath = Common.currentDrink.Link;
            selected_category = Common.currentDrink.MenuId;
        }

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        menuSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selected_category = menu_data_for_get_key.get(menu_data.get(position));
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct();
            }
        });

        setSpinnerMenu();
        setProductInfo();

    }

    private void setProductInfo() {
        if(Common.currentDrink != null){
            nameEditText.setText(Common.currentDrink.Name);
            priceEditText.setText(Common.currentDrink.Price);
            descriptionEditText.setText(Common.currentDrink.Description);
            Picasso.with(this).load(Common.currentDrink.Link).into(browserImageView);
            menuSpinner.setSelectedIndex(menu_data.indexOf(menu_data_for_get_value.get(Common.currentCategory.getID())));
        }
    }

    private void deleteProduct() {
        compositeDisposable.add(mService.deleteProduct(Common.currentDrink.ID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        Toast.makeText(UpdateProductActivity.this, s, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(UpdateProductActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }));
    }

    private void updateProduct() {
        compositeDisposable.add(mService.updateProduct(Common.currentDrink.ID,
                nameEditText.getText().toString(),
                uploadedImgPath,
                priceEditText.getText().toString(),
                descriptionEditText.getText().toString(),
                selected_category).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        Toast.makeText(UpdateProductActivity.this, s, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(UpdateProductActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }));
    }

    //Crtl+O
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                browserImageView.setImageURI(selectedFileUri);
                uploadFileToServer();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(UpdateProductActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(UpdateProductActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(UpdateProductActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(UpdateProductActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    private void uploadFileToServer() {

        if(selectedFileUri != null){

            File file = FileUtils.getFile(this,selectedFileUri);

            try {
                compressedImageFile = new Compressor(UpdateProductActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(50)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }


            String fileName = new StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(compressedImageFile.toString()))
                    .toString();
            ProgressRequestBody requestFile = new ProgressRequestBody(compressedImageFile,this);

            final MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    mService.uploadCategoryFile(body)
                            .enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    //After uploaded we will get file name and return String contains link of image
                                    uploadedImgPath = new StringBuilder(Common.BASE_URL)
                                            .append("server/category/category_img/")
                                            .append(response.body().toString())
                                            .toString();
                                    Log.d(TAG, "onResponse: IMGPATH " + uploadedImgPath);
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Toast.makeText(UpdateProductActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }).start();
        }
    }

    private void setSpinnerMenu() {
        for(Category category : Common.menuList){ //Common.menuList lo seteamos = categories en HomeActivity
            menu_data_for_get_key.put(category.getName(),category.getID());
            menu_data_for_get_value.put(category.getID(),category.getName());

            menu_data.add(category.getName());

            menuSpinner.setItems(menu_data);
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
