package com.example.nicolas.drinkshopserver.Interface;

import android.view.View;

public interface IItemClickListener {

    void onClick(View view, boolean isLongClick);
}
