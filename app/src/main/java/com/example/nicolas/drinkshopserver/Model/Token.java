package com.example.nicolas.drinkshopserver.Model;

public class Token {
    public String Phone,Token,IsServerToken;

    public Token() {
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        this.Phone = phone;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        this.Token = token;
    }

    public String getIsServerToken() {
        return IsServerToken;
    }

    public void setIsServerToken(String isServerToken) {
        this.IsServerToken = isServerToken;
    }
}
