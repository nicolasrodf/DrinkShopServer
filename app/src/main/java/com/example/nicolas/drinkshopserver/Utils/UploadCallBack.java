package com.example.nicolas.drinkshopserver.Utils;

/**
 * Created by Nicolas on 24/05/2018.
 */

public interface UploadCallBack {
    void onProgressUpdate(int percentage);
}
