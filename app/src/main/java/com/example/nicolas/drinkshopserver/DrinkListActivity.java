package com.example.nicolas.drinkshopserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nicolas.drinkshopserver.Adapter.DrinkListAdapter;
import com.example.nicolas.drinkshopserver.Model.Drink;
import com.example.nicolas.drinkshopserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkshopserver.Utils.Common;
import com.example.nicolas.drinkshopserver.Utils.ProgressRequestBody;
import com.example.nicolas.drinkshopserver.Utils.UploadCallBack;
import com.ipaulpro.afilechooser.utils.FileUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DrinkListActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "DrinkListActivity";

    private static final int PICK_FILE_REQUEST = 9998;

    IDrinkShopAPI mService;
    RecyclerView drinksRecycler;

    FloatingActionButton addButton;

    ImageView browserImageView;
    EditText drinkNameEditText, drinkPriceEditText, descriptionEditText;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    Uri selectedFileUri = null;
    String uploadedImgPath = "";
    File compressedImageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_list);

        mService = Common.getAPI();

        addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDrinkDialog();
            }
        });

        drinksRecycler = findViewById(R.id.drinks_recycler);
        drinksRecycler.setLayoutManager(new GridLayoutManager(this,2));
        drinksRecycler.setHasFixedSize(true);

        loadDrinkList(Common.currentCategory.getID());
    }

    private void showAddDrinkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar Nuevo Producto");

        View view = LayoutInflater.from(this).inflate(R.layout.add_new_product_layout,null);

        drinkNameEditText = view.findViewById(R.id.drink_name_edit_text);
        drinkPriceEditText = view.findViewById(R.id.drink_price_edit_text);
        descriptionEditText = view.findViewById(R.id.drink_desc_edit_text);
        browserImageView = view.findViewById(R.id.browser_image_view);

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        //Set View
        builder.setView(view);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                uploadedImgPath = "";
                selectedFileUri = null;
            }
        }).setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(drinkNameEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese el nombre del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(drinkPriceEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese el precio del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(descriptionEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese la descripcion del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(uploadedImgPath.isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor seleccione la imagen del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                compositeDisposable.add(mService.addNewProduct(drinkNameEditText.getText().toString(),
                        uploadedImgPath,
                        drinkPriceEditText.getText().toString(),
                        descriptionEditText.getText().toString(),
                        Common.currentCategory.ID)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                Toast.makeText(DrinkListActivity.this, s, Toast.LENGTH_SHORT).show();
                                loadDrinkList(Common.currentCategory.getID()); //refresh page
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(DrinkListActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }));

            }
        }).show();

    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(DrinkListActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(DrinkListActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(DrinkListActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                browserImageView.setImageURI(selectedFileUri);
                uploadFileToServer();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(DrinkListActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    private void uploadFileToServer() {

        if(selectedFileUri != null){

            File file = FileUtils.getFile(this,selectedFileUri);

            try {
                compressedImageFile = new Compressor(DrinkListActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(50)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = new StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(compressedImageFile.toString()))
                    .toString();
            ProgressRequestBody requestFile = new ProgressRequestBody(compressedImageFile,this);

            final MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    mService.uploadProductFile(body)
                            .enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    //After uploaded we will get file name and return String contains link of image
                                    uploadedImgPath = new StringBuilder(Common.BASE_URL)
                                            .append("server/product/product_img/")
                                            .append(response.body().toString())
                                            .toString();
                                    Log.d(TAG, "onResponse: IMGPATH " + uploadedImgPath);
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Toast.makeText(DrinkListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }).start();
        }
    }

    private void loadDrinkList(String id) {
        compositeDisposable.add(mService.getDrink(id).observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<List<Drink>>() {
            @Override
            public void accept(List<Drink> drinks) throws Exception {
                displayDrinkList(drinks);
            }
        }));
    }

    private void displayDrinkList(List<Drink> drinks) {
        DrinkListAdapter adapter = new DrinkListAdapter(this,drinks);
        drinksRecycler.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        loadDrinkList(Common.currentCategory.getID());
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
