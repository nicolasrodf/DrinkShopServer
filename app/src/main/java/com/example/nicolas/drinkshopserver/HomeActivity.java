package com.example.nicolas.drinkshopserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nicolas.drinkshopserver.Adapter.MenuAdapter;
import com.example.nicolas.drinkshopserver.Model.Category;
import com.example.nicolas.drinkshopserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkshopserver.Utils.Common;
import com.example.nicolas.drinkshopserver.Utils.ProgressRequestBody;
import com.example.nicolas.drinkshopserver.Utils.UploadCallBack;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ipaulpro.afilechooser.utils.FileUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UploadCallBack {
    private static final String TAG = "HomeActivity";

    private static final int REQUEST_PERMISSION_CODE = 9999;
    private static final int PICK_FILE_REQUEST = 9998;
    RecyclerView recyclerMenu;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    IDrinkShopAPI mService;

    EditText nameEditText;
    ImageView browserImageView;

    Uri selectedFileUri = null;
    String uploadedImgPath = "";
    File compressedImageFile;

    //Ctrl+O
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case REQUEST_PERMISSION_CODE:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Permission otorgado", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Permission denegado", Toast.LENGTH_SHORT).show();
            }
            break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("READY BEBIDAS");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddCategoryDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //View
        recyclerMenu = findViewById(R.id.menu_recycler);
        recyclerMenu.setLayoutManager(new GridLayoutManager(this,2));
        recyclerMenu.setHasFixedSize(true);

        mService = Common.getAPI();

        getMenu();

        updateTokenToServer();
    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }



    private void updateTokenToServer() {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        mService.updateToken("server_app_01",
                                instanceIdResult.getToken(),
                                "1")
                                .enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        Log.d(TAG, "onResponse: " + response.body());
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                        Log.d(TAG, "onFailure: "+ t.getMessage());
                                    }
                                });
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showAddCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar Nueva Categoría");

        View view = LayoutInflater.from(this).inflate(R.layout.add_category_layout,null);

        nameEditText = view.findViewById(R.id.name_edit_text);
        browserImageView = view.findViewById(R.id.browser_image_view);

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        //Set View
        builder.setView(view);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                uploadedImgPath = "";
                selectedFileUri = null;
            }
        }).setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(nameEditText.getText().toString().isEmpty()){
                    Toast.makeText(HomeActivity.this, "Por favor ingrese el nombre de la categoria", Toast.LENGTH_SHORT).show();
                    return;
                } else if(uploadedImgPath.isEmpty()){
                    Toast.makeText(HomeActivity.this, "Por favor seleccione la imagen de la categoria", Toast.LENGTH_SHORT).show();
                    return;
                }
                compositeDisposable.add(mService.addNewCategory(nameEditText.getText().toString(),uploadedImgPath)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        Toast.makeText(HomeActivity.this, s, Toast.LENGTH_SHORT).show();
                        getMenu(); //show menu again (refresh page)
                        uploadedImgPath = "";
                        selectedFileUri = null;
                    }
                }));
            }
        }).show();

    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(HomeActivity.this, "Permission denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                browserImageView.setImageURI(selectedFileUri);
                uploadFileToServer();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(HomeActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private void uploadFileToServer() {

        if(selectedFileUri != null){

            File file = FileUtils.getFile(this,selectedFileUri);

            try {
                compressedImageFile = new Compressor(HomeActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(50)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }


            String fileName = new StringBuilder(UUID.randomUUID().toString())
                    .append(FileUtils.getExtension(compressedImageFile.toString()))
                    .toString();
            ProgressRequestBody requestFile = new ProgressRequestBody(compressedImageFile,this);

            final MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file",fileName,requestFile);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    mService.uploadCategoryFile(body)
                            .enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    //After uploaded we will get file name and return String contains link of image
                                    uploadedImgPath = new StringBuilder(Common.BASE_URL)
                                        .append("server/category/category_img/")
                                        .append(response.body().toString())
                                        .toString();
                                    Log.d(TAG, "onResponse: IMGPATH " + uploadedImgPath);
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }).start();
        }
    }

    private void getMenu() {
        compositeDisposable.add(mService.getMenu().observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<List<Category>>() {
            @Override
            public void accept(List<Category> categories) throws Exception {
                displayMenuList(categories);
            }
        }));
    }

    private void displayMenuList(List<Category> categories) {

        Common.menuList = categories;

        MenuAdapter adapter = new MenuAdapter(this, categories);
        recyclerMenu.setAdapter(adapter);
    }

    //Ctrl+O

    @Override
    protected void onResume() {
        getMenu();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_show_order) {
            startActivity(new Intent(HomeActivity.this,ShowOrderActivity.class));

        } else if (id == R.id.nav_message) {
            startActivity(new Intent(HomeActivity.this,SendMessageActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
