package com.example.nicolas.drinkshopserver;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nicolas.drinkshopserver.Adapter.OrderDetailAdapter;
import com.example.nicolas.drinkshopserver.Adapter.OrderViewAdapter;
import com.example.nicolas.drinkshopserver.Model.Cart;
import com.example.nicolas.drinkshopserver.Model.DataMessage;
import com.example.nicolas.drinkshopserver.Model.MyResponse;
import com.example.nicolas.drinkshopserver.Model.Order;
import com.example.nicolas.drinkshopserver.Model.Token;
import com.example.nicolas.drinkshopserver.Model.User;
import com.example.nicolas.drinkshopserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkshopserver.Retrofit.IFCMService;
import com.example.nicolas.drinkshopserver.Utils.Common;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderDetailActivity extends AppCompatActivity {
    private static final String TAG = "ViewOrderDetailActivity";

    TextView txt_order_id, txt_name, txt_phone, txt_order_price, txt_order_address, txt_order_comment;
    Spinner orderStatusSpinner;

    RecyclerView orderRecyclerView;

    //Declare values for spinner
    String[] spinnerSource = new String[]{
            "Cancelado", //index 0
            "Solicitado", //1
            "En Proceso", //2
            "En Camino", //3
            "Finalizado", //4
    };

    IDrinkShopAPI mService;
    IFCMService mFCMService;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    String name;

    ImageView messageImageView, phoneImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        setTitle("READY BEBIDAS");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mService = Common.getAPI();
        mFCMService = Common.getFCMApi();

        //Send whatsapp msg
        messageImageView = findViewById(R.id.msg_image_view);
        messageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            }
        });

        //Make phonecall
        phoneImageView = findViewById(R.id.phone_image_view);
        phoneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ViewOrderDetailActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", Common.currentOrder.getUserPhone(), null));
                startActivity(intent);
            }
        });

        txt_order_id = findViewById(R.id.order_id_text_view);
        txt_name = findViewById(R.id.name_text_view);
        txt_phone = findViewById(R.id.phone_text_view);
        txt_order_price = findViewById(R.id.order_price_text_view);
        txt_order_address = findViewById(R.id.order_address_text_view);
        txt_order_comment = findViewById(R.id.order_comment_text_view);
        orderStatusSpinner = findViewById(R.id.order_status_spinner);

        orderRecyclerView = findViewById(R.id.recycler_order_detail);
        orderRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        orderRecyclerView.setAdapter(new OrderDetailAdapter(this));

        mService.getUserInformation(Common.currentOrder.getUserPhone())
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        name = response.body().getName();
                        txt_name.setText(name);
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        txt_order_id.setText(new StringBuilder("#").append(Common.currentOrder.getOrderId()));
        txt_phone.setText(Common.currentOrder.getUserPhone());
        txt_order_price.setText(new StringBuilder("$ ").append(Common.currentOrder.getOrderPrice()));
        txt_order_address.setText(Common.currentOrder.getOrderAddress());
        txt_order_comment.setText(Common.currentOrder.getOrderComment());



        //Set Array adapter for order status Spinner (for place data)
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                spinnerSource);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderStatusSpinner.setAdapter(spinnerArrayAdapter);

        setSpinnerSelectedBaseOnOrderStatus();



    }

    private void setSpinnerSelectedBaseOnOrderStatus() {

        switch (Common.currentOrder.getOrderStatus())
        {
            case -1:
                orderStatusSpinner.setSelection(0); //Cancelled
                break;
            case 0:
                orderStatusSpinner.setSelection(1); //Placed
                break;
            case 1:
                orderStatusSpinner.setSelection(2); //Processed
                break;
            case 2:
                orderStatusSpinner.setSelection(3); //Shipping
                break;
            case 3:
                orderStatusSpinner.setSelection(4); //Shipped
                break;
        }
    }

    //Ctrol+O
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_detail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_save_order_detail)
            saveUpdateOrder();
        return true;
    }

    private void saveUpdateOrder() {

        final int order_status = orderStatusSpinner.getSelectedItemPosition()-1;
        compositeDisposable.add(mService.updateOrderStatus(Common.currentOrder.getUserPhone(),
                Common.currentOrder.getOrderId(),
                order_status)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                sendOrderUpdateNotification(Common.currentOrder, order_status);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Log.d(TAG, "accept: ERRROR: " + throwable.getMessage());
            }
        }));
    }

    private void sendOrderUpdateNotification(final Order currentOrder, final int order_status) {

        mService.getToken(currentOrder.getUserPhone(),
                "0")
                .enqueue(new Callback<Token>() {
                             @Override
                             public void onResponse(Call<Token> call, Response<Token> response) {
                                //When we have Token , just send notification to this token
                                 Token userToken = response.body();
                                 DataMessage dataMessage = new DataMessage();
                                 Map<String,String> contentSend = new HashMap<>();
                                 contentSend.put("title","Tu orden tiene un nuevo status:");
                                 contentSend.put("message", "Orden #: " + currentOrder.getOrderId() + " ha sido actualizada a "+
                                         Common.convertCodeToStatus(order_status));
                                 //Toast.makeText(ViewOrderDetailActivity.this, "TOEKN; " + userToken.getToken(), Toast.LENGTH_SHORT).show();
                                 if(userToken.getToken() != null)
                                     dataMessage.setTo(userToken.getToken());
                                 dataMessage.setData(contentSend);
                                 mFCMService.sendNotification(dataMessage)
                                     .enqueue(new Callback<MyResponse>() {
                                         @Override
                                         public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                             Log.d(TAG, "onResponse: CODE " +response.code());
                                             if(response.code() == 200) {
                                                 Log.d(TAG, "onResponse: SUCCESS  " + response.body().success);
                                                 if (response.body().success == 1) {
                                                     Toast.makeText(ViewOrderDetailActivity.this, "Order updated!!", Toast.LENGTH_SHORT).show();
                                                     finish();
                                                 } else {
                                                     Log.d(TAG, "onResponse: SEND FAILED");
                                                     Toast.makeText(ViewOrderDetailActivity.this, "Send notification failed !", Toast.LENGTH_SHORT).show();
                                                 }
                                             }

                                         }

                                         @Override
                                         public void onFailure(Call<MyResponse> call, Throwable t) {
                                             Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                         }
                                     });

                             }

                             @Override
                             public void onFailure(Call<Token> call, Throwable t) {
                                 Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                             }
                         });


    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}
